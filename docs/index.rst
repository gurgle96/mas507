.. MAS507 documentation master file, created by
   sphinx-quickstart on Tue Aug 25 21:04:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MAS507 Project 2020
===================
.. image:: ./figs/uia.png
   :width: 600px
   :align: center
   :alt: alternate text

Development of Strawberry-picking Robot
==================================================

.. image:: ./figs/Render_final_ISO.png
   :width: 15cm

Student Group
-------------

Jørgen Nilsen,
Daniel Råmundsen,
Henry Thorleif Langeland Jenssen and
Øystein Rott

Lecturers
---------

- Sondre Sanden Tørdal, PhD
   - Postdoctoral Fellow in Mechatronics
   - https://www.uia.no/kk/profil/sondrest

- Mette Mo Jakobsen, PhD
   - Professor in Product Development and Project-based learning
   - https://www.uia.no/kk/profil/mettemj


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   src/introduction
   src/theory
   src/projectManagement
   src/productdevelopment
   src/method
   src/results
   src/discussion
   src/conclusion
   src/citation
   src/appendix
   

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
