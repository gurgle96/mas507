Conclusion
==========

The aim of this project was to develop and design a device for harvesting strawberries autonomously by combining product development methods with a practical project. 
Suitable solutions in the concept and embodiment phase where developed in the product development part of the project. Brainstorming, different charts and a criteria 
matrix was used to find a proper concept with the best design to fulfill the different task criteria. The embodiment phase resulted in a final concept choice where functionalities was 
considered as the most important criteria. Because of some complications when testing at the beginning, the jetbot was re-designed to be able to fulfill the task in a proper way. 
All the codes was build up based on the templates that was given, with some small changes and adds. In the end, the harvesting jetbot managed to complete the task and harvest only the red strawberries, 
even though difficulties with ArUco markers and lighting was present. It completed the harvesting in a time that was well within the three minutes, given in the description, with all the strawberries in the jetbot.         