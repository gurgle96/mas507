Citation
===========================

.. [CIT01] M. M. Jakobsen,Produktutvikling:  Verktøykasse  for  utvikling  av  konkurransedyktigeprodukter.  Frydenlundsvej 30 A, 2950 Vedbæk:  Fortuna Forlag, 1997 
            (Book)

.. [CIT02] Gitlab,
            https://www.simplilearn.com/tutorials/git-tutorial/what-is-gitlab. Downloaded: 2020.11.08
 
.. [CIT03] ROS Topics,
            http://wiki.ros.org/Topics. Downloaded: 2020.11.10

.. [CIT04] Roscore,
            http://wiki.ros.org/roscore. Downloaded: 2020.11.10

.. [CIT05] Rosmaster,
            http://wiki.ros.org/Master. Downloaded: 2020.11.10

.. [CIT06] XML,
              https://www.w3schools.com/xml/xml_whatis.asp. Downloaded: 2020.11.10

.. [CIT07] Ros Packages,
              http://wiki.ros.org/Packages. Downloaded: 2020.11.10

.. [CIT08] Python,
             https://www.python.org/doc/essays/blurb/. Downloaded: 2020.11.11

.. [CIT09] Open CV,
             https://opencv.org/about/. Downloaded: 2020.10.11


.. [CIT10] Trello info page,
            https://help.trello.com/article/708-what-is-trello. Downloaded: 2020.10.16


.. [CIT11] Repository, Group 3,
            https://gitlab.com/gurgle96/mas507.

            
.. [CIT12] Repository, MAS507,
            https://gitlab.com/uia-mekatronikk/mas507. Downloaded: Multiple times


.. [CIT13] Web page, MAS507,
            https://uia-mekatronikk.gitlab.io/mas507/index.html. Downloaded: Multiple times
