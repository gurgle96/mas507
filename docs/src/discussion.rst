Discussion
==========

ArUco markers and lighting
--------------------------

During initial testing of ArUco marker detection at Campus Fjære, it was 
noticed that both the lighting conditions and background of the marker affected 
the detection and made it difficult for the detection algorithm. Typical symptoms 
of improper lighting were wrong distance measurement; the detected positions of 
the ArUco was not coherent with the actual physical distance and that the ArUco 
was not detected at all. When a new marker was detected, usually the first three 
detections would give a small non-zero x, y and z distance.

Also worth noting, is that in the early stages of code development for controlling 
the robot around the strawberry plants, the robot was programmed to continue turning 
clockwise at a set velocity when it reached a certain distance from the marker. Due 
to the relatively slow framerate of the detection, this led to the detection algorithm 
only seeing the blur of a ArUco marker and not being able to detect it. This was solved 
by programming the robot to only turn for a set period, thus making it turn a determined 
angle. It would now be at a standstill, looking towards the new ArUco marker. 

To improve the success rate for detecting the ArUco markers, they were placed around 
the track with a slight angle backwards, allowing light to directly hit the 4x4 matrix 
that the marker is constructed from. Another improvement was to have a uniform background 
around the marker. Despite these efforts, the problem with scattered lighting conditions at 
Campus Fjære decreased the performance of the camera compared to those at Campus Grimstad. 
The final testing and resulting strawberry picking were performed at Campus Grimstad, where 
the lights are mounted higher in the ceiling.

Number of red strawberries
--------------------------

Due to the lack of red strawberries during final testing, the strawberry picking was done with only 
6 red berries. This has been decleared with the supervisors. 


Wi-Fi
-----------
While testing the jetbot remotely, the internet condition at Campus Fjære was quite challenging 
at times. This meant that the jetbot often lost its internet connection while driving, which 
again led to difficulties with programming and tuning the jetbot necessary. Due to these circumstances the jetbot driving were moved to Campus Grimstad, as mentioned in 
the section above, 
were also the internet condition was favorable. This led to a much more stable connection for the jetbot, which again eased the process with programming, tuning and driving the jetbot between ArUco markers, around bushes and towards strawberries.             



Product development
-------------------

As mentioned previously, the options for developing the robot was fairly limited. There was only the robot's arm that was conceptualized and developed. 
When performing a product development process, there are many different options to consider. The methods chosen for developing the final product were proven successful in this case. 
However, it was not necessarily the only, or even the optimal solution. In theory, there are other methods which might have been favorable in regards to different aspects. 
Given that the functionality was deemed the most important point, the approach of using a function tree was considered advantageous. For example, if marked demands was the top priority, 
this would perhaps not have been the best approach for solving the task. 

Four concepts were generated and evaluated. The evaluation process where the concepts were ranked in regards to different criteria resulted in one final concept. 
The chosen concept had to undergo changes in order to achieve a level of functionality that was desired. This proved that there are solutions that may have seemed adequate, 
but did not work as expected. Product development was shown to be an iterative process, with several potential solutions. Concept generation was a useful tool for initializing the project, 
but prototyping and testing was equally important for achieving a successful end product.

