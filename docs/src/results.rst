Results
=======

The final design of the jetbot included a basket for the strawberries, an arm for picking the strawberries and a mounting device for the servo motor. Only one motor was used to control the lifting arm. This was attached to the front plate of the jetbot skeleton with a 3D printed clips, designed to easily be attached and easily could be moved to a more suitable place on the plate if appropriate. 

.. figure:: ../figs/servo_mount_ISO.png 
    :width: 17cm
    :alt: 
    :align: center

A basket was designed and 3D printed to follow the lines of the jetbot and the wheels. It have two screw holes on each side at the front and one in the middle at the back. In addition to have enough space for storing strawberries, it contained also an extra room for the battery on the left side of the jetbot that was separated from the rest.

.. figure:: ../figs/basket_ISO.png
    :width: 17cm
    :alt: 
    :align: center

To effectuate the production, an arm was designed and 3D printed in one part. This was mounted to the basket and the servo mount respectively. The arm was hinged to the basket, that acted as a revolution point for the arm. On the underside, the arm was mounted to a horn on the servo with a welding wire with sufficient length to use full angular range on the servo motor.

.. figure:: ../figs/spoon_over.png
    :width: 17cm
    :alt: 
    :align: center

.. figure:: ../figs/spoon_under.png
    :width: 17cm
    :alt: 
    :align: center    

Resulting finally assembled jetbot was small and compact. It consisted of few parts, where the parts were designed for both user method and production. Everything from designing parts so that it does not get in the way of bushes and camera views, to minimizing the use of support structure in 3D printing has been taken into account.            

.. figure:: ../figs/Render_final_ISO.png
    :width: 17cm
    :alt: 
    :align: center

.. figure:: ../figs/Render_final_FRONT.png
    :width: 17cm
    :alt: 
    :align: center

.. figure:: ../figs/Render_final_TOP.png
    :width: 17cm
    :alt: 
    :align: center

The fully assembled jetbot managed to maneuver through the track in just under 100 seconds. It used the distance from the three ArUco markers to drive, turn and look for strawberries and other Arucos. All the green strawberries were ignored, and all the red strawberries were picked and placed in the jetbot. The video from the driving and picking can be viewed with the below.   

|
.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/jY4uU1DkNp0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>