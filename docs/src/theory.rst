Theory
======

Product Development 
---------------------------
There are several options that can be considered when it comes to the development of an autonomous strawberry-picking robot. 
Therefore, in order to develop a suitable solution, methods of product development are utilized. A product development process can be solved
in many different ways, and there are several methods that can be applied to this project. However, to limit the scope of the task, only the 
methods deemed most suitable are chosen for the development process. The nine dimensions of product quality are used as a foundation for ensuring 
that the product is able to achieve a desirable level of operation. These dimensions are functionality, use, production, environment, product life, 
market, aesthetic design, safety and added value. However, given the scope of the project the dimensions are given different levels of priority based on how relevant they are. 


.. figure:: ../figs/9_Dimensions.png
    :width: 15cm
    :alt: 
    :align: center

* Functionality dimension covers the technical aspects of the product. In the case of a strawberry-picking autonomous robot, the functionality aspect has to be considered thoroughly. 
First, the robot and the robots arm have to be able to move. It also needs a functional vision system for its own orientation, as well as for finding the strawberry. 
It has to be able to separate between red and yellow berries, and only pick the red ones. The operating time for the robot should be minimized, as long as this does not affect its ability to perform the operation successfully. 
Functionality is therefore highly prioritized in this project.  [CIT01]_


* Market dimension deals with the marketability of the product. Given the nature of this project, there is no actual market to develop this product for. However, instead of traditional customers, the course supervisors will be viewed as an artificial market. The product will be developed in order to satisfy their requirements, as well as focusing on creative and unique solutions to meeting the user requirements.

* Production dimension is about the resources required in order to create a product with a strong internal quality. The only part of the product which is not predetermined is the robots arm. The factors that can be considered in this aspect is the size and sturdiness of the components, the difficulty of assembling, as well as the number of servo-motors required for operation. 


* Aesthetic design dimension covers the visual end result of the product. A visually pleasing design can be a deciding factor for a potential customer, but as part of this project it is not a priority.   

* The use dimension covers a products end user experience. The end-product should be able to be used without much difficulty by all potential users, in this case the developers and the technical supervisor. This product should be fully autonomous when fully developed, and therefore require minimal interaction with the users. 

* Safety dimension is related to making sure a product is not able to damage humans, the environment or anything in it's surroundings. Given the size and power of the robot in this project, there is no likelihood of any significant damage being afflicted on any humans. An aspect that can be considered in this dimension is potential damage done to the strawberries or the strawberry bushes. 


* The environmental dimension covers how much of an environmental foot-print the product will leave behind. This factors in resources and energy required for development, operation and eventually disposal of the product. The robot arm designed for this product will be 3D-printed with a relatively small amount of plastic which can be recycled when the project is over. It will also only be requiring electrical energy in order to operate.  


* The added value dimension comprises of all the additions that comes with the product. Usually this would be for example information brochures, user manuals, service agreements or guarantees among other things. In this project, this written report can be perceived as a combination of an information brochure and a user manual. This dimension does not affect the development process. 


* The product life dimension considers the products life-cycle from the first to the last time it is used. Different products have different expectations for how long they should be functional. It is important to define this to potential customers, and ensuring that the product is able to meet these expectations. As for this project, the expected lifetime of the robot is relatively short, but it is important to deliver a product that can operate properly during this time. [CIT01]_



Brainstorming
"""""""""""""""
Brainstorming is a method that can be used to generate as many as possible ideas on how to solve a problem. A group of people is tasked to present
every solution that occur to them, within an established time frame. The ideas that are presented shall not be evaluated or discussed during this
process. The ideas are written down, and sketches can be made to visualize the solutions. Afterwards, the ideas can be evaluated and potentially be further developed. 

.. figure:: ../figs/brainstorming.svg
    :width: 400cm
    :alt: 
    :align: center


Function tree 
"""""""""""""""
One method that can be used when developing concepts is the function tree method. This method helps with providing an overview and 
identifying the key functions that are desired. When the functions are identified, brainstorming is used to generate different solutions to
each of the functions. Then an evaluation process determines which of the solutions are most suitable to achieve the necessary functions. From the 
selected solutions, sub-functions can be identified and the process can be repeated until the desired level of detail is achieved. 

.. figure:: ../figs/function_tree.svg
    :width: 600cm
    :alt: 
    :align: center

Product concepts
"""""""""""""""""



The purpose of this method is to generate, document and choose
solutions for sub-concepts and total-concepts, that are combinations of sub-concepts to
suitable total-concepts. By generating concepts from scratch and comparing them to each
other, it can provide a overview of pros and cons. By analyzing them in regards to the 9
dimensions, a final choice of concept could be chosen based on priorities.

.. figure:: ../figs/product_concept.svg
    :width: 600cm
    :alt: 
    :align: center



Jetbot 
------

The base of the robot is a Nvidia Jetbot, a open-source platform designed by Nvidia for creating AI and vision driven 
applications. 3D printing is used to manufacture the chassis of the robot, which makes it easy to modify and customize. 
360 degree servos are used to drive the wheels of the robot and a caster wheel is mounted, giving the robot three points of contact.
As the brain of the robot is the Nvidia Jetson Nano, a small powerful computer designed for computing multiple neural networks in parallel. 
Tasks like image classification and object detection is just one of many things the Jetson Nano is capable of. 
It features a 128 Cuda cores NVIDIA Maxwell architecture GPU, quad core ARM Cortex processor and 4 GB of memory.


.. figure:: ../figs/jetbot.png
    :width: 25cm
    :alt: 
    :align: center

Gitlab
---------------------------
Gitlab is a web-based hosting service generally used for version control of code.  Version control allows for saving and logging changes made to a project.  By using version control,  it is possible to track all the specific changes that has been made,  when they were made and who made them.   This makes GitLab widely used for a variety of projects,  as it provides the ability to track potential errors that might occur and find out when they were made.  For projects involving multiple people, Gitlab offers everyone in a group to synchronize their code, making sure all participants has the latest version.  Moreover, if a repository is made public, it enables users to view the codes that has been created and take inspiration. [CIT02]_


.. figure:: ../figs/git.png
    :width: 12cm
    :alt: 
    :align: center


OpenCV
---------------------------

Open source computer vision (OpenCV ) is a computer vision library which provides a broad range of image analysis and manipulation tools. 
In this project, the python implementation ofOpenCV is used. When processing an image, it is interpreted as a matrix were each pixel is 
represented by a value between 0 and 255. Grayscale images are used to improve calculation speed and simplify the process, since a grayscale 
image only use one value per pixel instead of three in RGB. This allows for mathematical manipulation of the image.  In a videostream, each 
frame of the video is handled as a separate image and processed in real-time. [CIT09]_

.. figure:: ../figs/OPENCV.png
    :width: 7cm
    :alt: 
    :align: center






ROS
---------------------------
Robot Operating system (ROS) is an open source software framework that provides features which enables communication between hardware devices 
and software components.  This makes ROS well suited for robotics applications, as it can assist with interfacing hardware with advanced software 
features such as computer vision, motion planning, simulation and artificial intelligence related tasks. ROS mainly supports programming with C++ and Python through the libraries roscpp and rospy. 
An important aspect of ROS is how information is transmitted. The information is communicated between nodes. A node is a process that performs computations. When information is sent or received, 
a node will either subscribe or publish to a topic.  A topic is where the in-formation goes through.  The data type a topic is holding is called a message.  
This could be for example a vector containing a set of floats describing the position of an object.  When a node is sending information through a topic, it publishes.  
When a node is receiving information from a topic, it subscribes to that topic. A node can also request or reply with another node. 
Instead of connecting them through a topic, a service is used. An overview of how dataflows between nodes is shown. [CIT03]_


.. figure:: ../figs/ros.svg
    :width: 600cm
    :alt: 
    :align: center




In order for the nodes to communicate with each other, roscore has to be running. Roscore is usually started automatically when any ROS programs are launched, 
but can also be started manually with the command :code:`roscore` in a terminal window. [CIT04]_ This will start up a ROS Master, ROS Parameter Server and a rosout logging node. 
The ROS Master enables individual ROS nodes to locate one another and tracks any publishers and subscribers to topics and services. The  Parameter  Server  is  what  nodes  
use  to  store  and  retrieve  parameters  at  runtime. When a script has been created,  it is recommended to create a launch file for it. A launch file makes it more convenient,  
as  it  start  up  multiple  nodes,  a  master,  setting  parameters, launches different scripts and more. [CIT05]_ They are written in xml, which is a software- and hardware-independent 
tool used for storing and transporting data. [CIT06]_ A typical launch file will include several attributes, such as the package of the node, the node type, the name of the node and more.  
Launch files can also launch other launch files by adding include file to the code, and specifying the location of the package the launch file is located in. 
The program will then search for the file and launch it. ROS has a wide selection of packages available. A package can contain nodes, ROS-independent library, a dataset, 
configuration files and third party piece of software which together can forma useful module. Most of the packages can be found on GitHub. GitHub has a large variety of packages 
made by both big companies and by the ROS community, and can be used by everyone as long as the GitHub repository is visible. [CIT07]_


Python
---------------------------


Python is a general purpose programming language designed to be easy to understand and fast to develop and prototype with. Python code can be structured into scripts, functions and modules.

 * A script is a list of instructions to perform. 
 * A function takes a certain type of information as input, and then processes or performs actions based on it. 
 *  A module is a library that contains a selection of functions that can be used by a script. Scripts and modules are files with the file :code:`extension.py`, while functions exist within scripts and modules. [CIT08]_


.. figure:: ../figs/python_logo.png
    :width: 12cm
    :alt: 
    :align: center




