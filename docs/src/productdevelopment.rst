Product Development
========================

To develop different concepts, a brainstorm where performed. This was done to find out which sub-sections that are needed in the 
project. A morphological chart were then made. The main focus was on how to pick, move and store the strawberries. Out of this 
were the different concepts developed.

.. image:: ../figs/function_matrix.PNG
    :width: 15cm
    :alt: Morphological chart
    :align: center

To create the different concepts, lines where sketched between the different ideas. 4 initial concepts came out from this phase.

.. image:: ../figs/function_matrix_1.PNG
    :width: 15cm
    :alt: Morphological chart with lies
    :align: center

The 4 initial concepts were given a color each:

* Concept 1: Green
* Concept 2: Red
* Concept 3: Blue
* Concept 4: Orange

Initial concepts
--------------------------------
All the initial concepts were illustrated with simple hand-sketches. 

Concept 1
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The first concept have a fork at the end of a combined arm/slide. After picking the strawberry, the arm lifts until it rolls back
into the basket. The floor of the basket has an incline to make space for all strawberries.

.. image:: ../figs/Concept1_OR.PNG
    :width: 13cm
    :alt: Concept 1
    :align: center

Concept 2 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This concept have a spoon similar to the first concept. After the strawberry is picked, the spoon rotates to place it in the basket,
located just below. For this to work, two servos are needed.

.. image:: ../figs/Concept2_OR.PNG
    :width: 10cm
    :alt: Concept 2
    :align: center

Concept 3
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
In this concept is the arm mounted in front of the robot and rotate in the horizontal plane. It is fitted with a spoon that picks the
strawberry during rotating. It continues to rotate until the spoon is located over the basket. When rotation stops, the strawberry
will fall into the box. 

.. image:: ../figs/Concept_HJ.jpg
    :width: 6cm
    :alt: Concept 3
    :align: center  

Concept 4
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The final initial concept have an arm similar to first concept. It have a spoon in the end, combined with a slider. The arm is located 
in front of the robot. After picking the strawberry, the arm lifts so is roll into the box, located on top of the robot.

.. image:: ../figs/Concept_JN.png
    :width: 13cm
    :alt: Concept 4
    :align: center  

Concept reduction
------------------------

To analyze which concept to take into embodiment phase, all concepts where evaluated by six different criteria. These criteria 
were ranked from 1 to 4, where lowest number is best. As seen in the table below, two of the concepts have almost similar score. 
These concepts where taken into the embodiment phase for further elaboration.

.. figure:: ../figs/Criteria_matrix.PNG
    :width: 12cm
    :alt: Criteria matrix
    :align: center  

    Criteria matrix for concept reduction

Embodiment phase
----------------

Concept 1 and 4 from the initial concept phase were discussed further with a more detailed perspective. It was focused on which qualities in each concept that was worth further elaboration. 

* In concept 1 is the basket mounted on the side and back. This makes mounting and travel height for the strawberries considered to be easier. The arm here is mounted directly onto the servo. 
* Concept 4 has the arm mounted in front of the wheel. This is better regarding space conflict with the wheel. The arm is connected to the servo with a link arm and mounted to the robot with a hinge.

By combine the best qualities from these two concepts, two new embodiment concepts arise. 

Embodiment Concept 1 (EC1)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

This concept resemble initial concept 1 quite much. The main difference is that the slider-arm has been moved forward of the wheel. This increase the movability of the arm and avoid any possible conflicts with the wheel. The arm is also mounted directly onto the servo. The strawberry picker has a design resembling a claw. 

.. image:: ../figs/Concept_Henry_ISO.png
    :width: 8cm
    :alt: Angle_HJ
    :align: left  

.. image:: ../figs/Concept_Henry_Top.png
    :width: 8cm
    :alt: Top_HJ
    :align: right  

Embodiment Concept 2 (EC2)
^^^^^^^^^^^^^^^^^^^^^^^^^^

Embodiment Concept 2 has also the slider-arm mounted in front of the wheel. It is mounted with a hinge onto the body. The slider is moved up and down with a servo that also is mounted to the robots body. 
To connect the servo to the slider-arm, a pushrod is used. It is easier to adjust the movement of the slider in this concept by adjusting the length of the lever. The strawberry harvester at the end of the 
slider-arm resemble an ice cream spoon.

.. image:: ../figs/Concept_Jorgen_ISO.png
    :width: 8cm
    :alt: Angle_JN
    :align: left  

.. image:: ../figs/Concept_Jorgen_Top.png
    :width: 8cm
    :alt: Top_JN
    :align: right

Embodiment evaluation
^^^^^^^^^^^^^^^^^^^^^

A new extended criteria analysis was performed. This can be seen in the table below. It was difficult to find criteria that could differentiate the concepts in the embodiment phase. 
This due to their similarities in the design. It was therefore decided to evaluate the two concepts with emphasis on their functionality. 
Both slider-arms with their respective strawberry harvester printed and mounted to the robot. A test regarding their ability to pick and store strawberries were then performed. 
This was done manually, without the robot driving nor the servo operating the arm. The robot was moved up to a strawberry plant and the arm was relocated to be able to pick a strawberry. 


.. figure:: ../figs/Point_matrix_EC.PNG
    :width: 12cm
    :alt: Point_matrix_EC
    :align: center

    Criteria Matrix for evaluating Embodiment Concepts

Choice of concept
^^^^^^^^^^^^^^^^^

During testing, is was seen that both the grabber and slider-arm in EC1 lacked functionality when collecting strawberries. It had problems harvesting and also lost the berries during roll down in the slider. 
It was concluded that the problems occurs from two main problems; design of grabber and slider-arm, and difficulties to move the arm in a controlled way. 
Since the arm is mounted directly onto the servo makes is harder to control. It also was a bit wobbly.

Due to the problems mentioned above, it was decided to use EC2 in further development.







