Appendix
======================

Group contract
--------------

`Download group contract`_

.. _download group contract: https://gitlab.com/gurgle96/mas507/-/blob/master/docs/appendix/MAS_507_Contract_group_3_signed.pdf

Code-files
--------------
`Download code source files`_

.. _Download code source files: https://gitlab.com/gurgle96/mas507/-/tree/master/src

CAD-files
--------------

Download links for CAD-models

.. image:: ../figs/Basket.gif
    :width: 9cm
    :alt: 
    :align: left
:download:`Basket <../appendix/container.SLDPRT>`

|
|
|
.. figure:: ../figs/Slide_claw_OR.gif
    :width: 9cm
    :alt: 
    :align: left
:download:`Slide and claw <../appendix/Slide_claw_group_3.zip>`

|
|
|
.. figure:: ../figs/Servo_mount.gif
    :width: 9cm
    :alt: 
    :align: left
:download:`Servo mount <../appendix/Servo_mount.SLDPRT>`

|
|
|
.. figure:: ../figs/Assembly_v2_OR.gif
    :width: 9cm
    :alt: 
    :align: left
:download:`Total assembly of the robot <../appendix/Assembly_group_3.zip>`