Project Management
===================

Trello 
-----------------

To be able to have an overview of all tasks and deadlines in the concept development process, a Trello board was created. 
Trello is a tool for organizing projects. It is a collaborating tool organized as a board. One can compare it with a white board with stickers arranged in several lists. The stickers can be for instance notes, documents, photos and diagrams. [CIT10]_

.. image:: ../figs/trello-logo-blue.PNG
    :width: 3cm
    :alt: 
    :align: right  

**Product Development board**

The project was divided into two Trello boards, Jetbot and Product Development (PD). The PD board is elaborated here and contain the following lists:  

* Backlog
* Next tasks
* Ongoing
* Finished w[n] (n = week number)

.. figure:: ../figs/trello_overview.PNG
    :width: 100%
    :alt: 
    :align: center

    Overview of Trello


All tasks were labeled with tags, corresponding to main subjects of the project:

.. figure:: ../figs/trello_tags.PNG
    :width: 4cm
    :alt: 
    :align: right

    Overview of tags in Trello

* BC: Boundary conditions
* BS: Brainstorming
* CD: Concept development
* EV: Evaluation
* DS: Design
* PT: Prototyping
* TS: Testing
* FP: Finished Product
* AD: Administrative 

|

Backlog
""""""""""""

.. figure:: ../figs/backlog.PNG
    :width: 6.4cm
    :alt: Backlog
    :align: left

    Backlog


The first list on the board is the Backlog, containing cards for each of the tags above. Each of these card have a list of bullit points. These points are tasks within each subject that needs to be performed. 

.. figure:: ../figs/backlog_card.PNG
    :width: 10cm
    :alt: Backlog card
    :align: right

    Example card in Backlog

|
|

Next tasks
""""""""""""

.. figure:: ../figs/next_tasks.PNG
    :width: 6.4cm
    :alt: Next tasks
    :align: right

    Example list of Next Tasks

As the product development process progress are tasks from the bullit point lists made into individual cards. These cards are first put in the "Next tasks" list. At this stage are members assigned.

|
|
|
|
|
|
|
|


Ongoing tasks
""""""""""""

.. figure:: ../figs/ongoing.PNG
    :width: 6.4cm
    :alt: Ongoing
    :align: right

    Example list of Ongoing Tasks

To keep track of which tasks that is in progress at any given time, a list of ongoing tasks is used. If tasks are dependent on each other at this stage, a due date is set on the card.

|
|
|
|
|
|
|
|
|
|
|
|
|
|
|


Finished tasks
""""""""""""

.. figure:: ../figs/finished_wnn.PNG
    :width: 6.4cm
    :alt: Finished
    :align: right

    Example list of Finished Tasks

After completing a task, they are moved into a list of finished tasks. A list of finished task is made for every week. This is done to be able to keep record of work done each week.
