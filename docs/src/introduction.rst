Introduction
============

Project description
---------------------------

This report is the result of the project given in the course MAS507 - Product Development and Project Management. The lectures in 
this course is mainly divided into two subjects; theory regarding product development and practical lectures on Mechatronics.

The project is to develop a prototype for harvesting strawberries autonomously. This is to be done by combining product development 
methods with a practical project. Team work and project management are essential tools for reaching a finalized product.


Boundary conditions
---------------------------
There are two sets of boundary conditions for the practical project. The first set are given in the task.

* As a base must a 3D printed frame with a NVIDIA Jetson Nano on top be used
* Missing hardware to be produced are a robotic gripper arm and a basket for storing the strawberries.
* A maximum of three servos can be used on the arm.
* The robot shall pick 8 strawberries in a field showed in the figure below.
* The time from touching the first berry until the last berry is picked is limited to three minutes.

.. figure:: ../figs/strawberry_field_layout.PNG
    :width: 8cm
    :alt: 
    :align: center

    Strawberry field layout

The other set is the boundary conditions made for making the robot perform as optimized as possible. 

* All parts must be designed so that they are able to be produced with 3D print.
* Overall width shall not exceed 20 cm.
* Overall length shall not exceed 35 cm.


.. Limitations
.. Which parts are needed to be produced


