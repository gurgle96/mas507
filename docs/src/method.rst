.. raw:: html

    <style> .blue {color:blue} </style>
    <style> .red {color:red} </style>
    <style> .green {color:#32CD32} </style>

.. role:: blue
.. role:: red
.. role:: green

Method
======

Changes done after embodiment phase
--------------------------------------------
After some trial and error with the chosen concept EC2 from the embodiment phase, there had 
to be done some design changes to have a fully operated strawberry picker. The spoon design 
was not appropriate to pick the strawberries from the bushes. Therefore, the design of the 
spoon was changed to look more like a claw, with small sharp pins pointing slightly backwards 
at the end of the spoon to get a good grip around the berries and unhook then from the bushes. 

.. figure:: ../figs/Render_final_FRONT.png
    :width: 15cm
    :alt: 
    :align: center

Another design change was the length of the slider-arm. The distance from the strawberries 
to the aruco marker was too large for the embodiment design slider-arm, which lead to that every 
time the robot turned against the strawberries, it hit the bushes. Based on this arm were shorten 
down by some centimeters and the lever arm attachment had to be slightly moved to run the servo 
properly. 

.. figure:: ../figs/Render_final_TOP.png
    :width: 15cm
    :alt: 
    :align: center

The last design change was the shape of the spoon wall and floor. To direct the 
strawberries down the slider-arm, the spoon wall and floor was re-designed with an angle. This 
also helped the camera to get a better view of the aruco markers and strawberries. The spoon wall
was also increased so that the picking arm was right in front of the camera, in order to ease 
the lifting process. 

.. figure:: ../figs/Render_final_ISO.png
    :width: 15cm
    :alt: 
    :align: center

Camera calibration
---------------------------
In order to detect both strawberries and aruco markers, the camera needed a good calibration 
so that the robot manage to navigate through the marked track and also manage to detect the 
correct color of the strawberries. The calibration process was started by taking 15-20 pictures 
from several angles with different lengths to a calibration checkerboard with the :code:`ImageSaver.py` code.

.. figure:: ../figs/checkerboard.png
    :width: 10cm
    :alt: 
    :align: center

Then the :code:`calibrateCamera.py` code inside the :code:`/catkin_ws/src/mas507/calibration` folder was executed. 
This code read all the usefully images and trashed the bad images. For the useful images it detected 
all the corners of the checkerboard for the different images. From these corners, the input data for 
the calibration was found which was a set of 3D object points in the real world and the 
corresponding 2D image points in the images. Then the calibration was executed with the function :code:`cv.calibrateCamera()`, 
which returned and saved camera matrix, distortion coefficients, rotation- and translation vectors in a :code:`intrinsicCalibration.npz` file. 
This file was then moved to :code:`~/catkin_ws/src/mas507/data/` and launched with the command to rectify the raw image loaded from the camera. 

Main program
------------

Strawberry detection
^^^^^^^^^^^^^^^^^^^^
Image processing was used to detect red strawberries and measure the distance to the strawberries 
using OpenCV and PnP algorithm. The first part was the detection of strawberry with OpenCV. With color 
segmentation, the camera can manage to isolate specific colors from the image, in our case isolate a 
red strawberry from the remaining surroundings in the image. The width of the strawberry was manually 
measured and written in the program, so that the code makes a small ellipse around each strawberry after 
the segmentation had been done. The second part used a PnP algorithm to calculate the location of the 
strawberries relative to the camera location. Both parts were executed in the :code:`StrawberryDetector.py` code. 
In that code there were also implemented a “box” around the strawberries for helping the camera to 
not detect strawberries that were too far out to the side. With the PnP algorithm an absolute vector 
with the shortest distance to the strawberry was calculated in the :code:`MainController.py` code. This was 
used to control that the robot always tracks the strawberry that is closest to the camera location and 
to control the distance to the strawberries in the picking process.

.. figure:: ../figs/Strawberry_picker.gif
    :width: 17cm
    :alt: 
    :align: center

Main program structure
^^^^^^^^^^^^^^^^^^^^^^

Program for controlling the robot around the strawberry plants and for checking if there is a red strawberry 
available at the specified locations are made using a sequential style program, which is designed for picking 5 plants. 
The robot will execute this program two times, first when the program is launched and the robot is at the start position, 
and secondly when the robot has reached the third ArUco marker and turned 90 degrees clockwise. The ArUco markers gives 
the robot a point of reference, and the control system for driving the robot to a certain distance uses the absolute value 
of the x,y and z – vector sum. The main program is executed in the :code:`MainController.py` node, this node also includes 
the classes and functions that are used in the sequential program.


.. figure:: ../figs/MAS507_program_flowchart.svg
    :width: 600cm
    :alt: 
    :align: center

Structure of the sequential program for moving the robot from plant to plant is based upon a switch-case, with a case 
being either move-to-setpoint, turn CW/CCW, determine presence of red strawberry, pick strawberry or move-backwards. 
Each case is ended by a set of requirements, so that the robot would finish the ongoing tasks before moving to the next one. 
This sequence is designed for picking 5 plants in a row, when the robot has reached the end of the first side it will reset 
the case-counter and start with the first plant. 


ROS topics
^^^^^^^^^^
Communication and sharing of data is done by publishing ROS topics and subscribing to them. The ROS nodes that make up the program 
have a set of topics that they either subscribe to or publish on. The datatypes to be transmittet in the messages are listed in a :code:`.msg` file
located in the :code:`~/msg` folder. The purpose of this file is to assign datatypes to the variables that are shared on the virtual bus. 
The topics listed below is used in the main program for controlling the robot. 

* :code:`arucodata`, ArUco coordinates and number. Published by :code:`JetbotCamera.py` and subscribed to by :code:`MainController.py`.
* :code:`servoSetpoints`, left and right setpoints for the servomotors. Published by :code:`MainController.py` and subscribed to by :code:`JetbotController.py`.



Position controller
^^^^^^^^^^^^^^^^^^^
The object :code:`class DistanceController(object):` is a class that contains three different functions, all in regards to controlling the position of the robot. 
The position of the robot from the ArUco markers are controlled using the function :code:`def regulatorAruco`, a proportional controller with the absolute value of the 
x,y and z – vector sum as distance feedback. The controller is given a position setpoint and returns the servo setpoints to drive the motors.
When a strawberry is detected the function :code:`def regulatorBerry`, is used to drive the robot up to a defined distance from the strawberry. The third and final function 
is :code:`def manual`, where the servos are given setpoints for a defined period of time, this is used to manouver the robot in a defined way. 

In :code:`MainController.py` a instance of the :code:`class DistanceController(object):` is constructed once with
:code:`distanceController = DistanceController(500,10,15,-15)` thus setting values for selfcontained parameters
:code:`def __init__(self,gainP,gainA,satUpper,satLower):` respectively. These parameters are kept constant throughout the executed program.

* :code:`gainP` is the proportional gain for the controll of the robots distance from the ArUco markers.
* :code:`gainA` is the proportional gain for heading control of the robot. 
* :code:`satUpper` is the upper saturation limit for the servo setpoint.
* :code:`satLower` is the lower saturation limit for the servo setpoint.


Implemented ArUco position controller:
:code:`velLeft,velRight = distanceController.regulatorAruco(0.99, arucoX, arucoY, arucoZ, arucoID, arucoNr, t, t_1)`
Where :code:`arucoNr,t,t_1` is the number for the target ArUco, the time in seconds since the loop started and the timestamp for when the case was entered, respectively.
The last input variable is only used when turning the robot 90 degrees CW or CCW in :code:`def manual`. 

    .. code-block::

        # Move to first plant
        if (nCase == 1): 
            velLeft,velRight = distanceController.regulatorAruco(0.99, arucoX, arucoY, arucoZ, arucoID, arucoNr, t, t_1)
            print(velLeft,velRight,nCase,absID)
            armSet = 200

            if (0.97 < absID < 1.01):
                t_2 = t
                nCase = 2


Implemented manual position controller: 
:code:`velLeft,velRight,tEnd = distanceController.manual(0,1.3,t,t_2)`
Where :code:`1.3,t,t_2` is the time duration to perform the turning, the program time and a timestamp for when the case is entered. 

    .. code-block::

        # Turn 90 degress CW
        if (nCase == 2): 
            velLeft,velRight,tEnd = distanceController.manual(0,1.3,t,t_2)
            print(velLeft,velRight,nCase,absID)
            
            if (t>tEnd):
                t_3 = t
                nCase = 3



Implemented strawberry position controller:
:code:`velLeft,velRight = distanceController.regulatorBerry(0.12,xBerry,yBerry,zBerry)`
Where :code:`0.12` is the target distance for the strawberry, so the gripper can grab it. 

    .. code-block::

        # Pick red strawberry
        if (absBerry !=0) and (absBerry>0.08):
            print("Red strawberry detected")
            velLeft,velRight = distanceController.regulatorBerry(0.12,xBerry,yBerry,zBerry)

            if (0.10 < absBerry < 0.14):
                t_5 = t
                nCase = 5





Other objects and functions implemented in :code:`MainProgram.py` is :code:`ArucoData`, :code:`Joystick` and :code:`AbsVector`. 
:code:`ArucoData` and :code:`Joystick` are both classes for defining a callback function for the ROS topics; :code:`arucodata` and :code:`servoSetpoints`. 
:code:`AbsVector` is a function for calculating the absolute length of the vector from the camera coordinate center to the ArUco coordinate center.




















