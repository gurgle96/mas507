#!/usr/bin/env python
"""
Main Control Node
"""
import time
import rospy
import numpy as np
import csv
import os.path
import os
from mas507.msg import ServoSetpoints,WebJoystick,ArucoMarker
from sensor_msgs.msg import Image
from StrawberryDetector import StrawberryDetector 


class ArucoData(object):
    def __init__(self):
        self.x1 = 0
        self.x2 = 0
        self.x3 = 0
        self.x4 = 0
        self.y1 = 0
        self.y2 = 0
        self.y3 = 0
        self.y4 = 0
        self.z1 = 0
        self.z2 = 0
        self.z3 = 0
        self.z4 = 0
        self.id1 = 0
        self.id2 = 0
        self.id3 = 0
        self.id4 = 0
        self.theta1 = 0
        self.theta2 = 0
        self.theta3 = 0
        self.theta4 = 0
        

    def callback(self, msg):
        self.x1 = msg.x1
        self.x2 = msg.x2
        self.x3 = msg.x3
        self.x4 = msg.x4
        self.y1 = msg.y1
        self.y2 = msg.y2
        self.y3 = msg.y3
        self.y4 = msg.y4
        self.z1 = msg.z1
        self.z2 = msg.z2
        self.z3 = msg.z3
        self.z4 = msg.z4
        self.id1 = msg.id1
        self.id2 = msg.id2
        self.id3 = msg.id3
        self.id4 = msg.id4
        self.theta1 = msg.theta1
        self.theta2 = msg.theta2
        self.theta3 = msg.theta3
        self.theta4 = msg.theta4

class Joystick(object):
    def __init__(self):
        self.x = 0
        self.y = 0

    def callback(self, msg):
        self.x = msg.x
        self.y = msg.y


    def __init__(self,wheelDia,centerDist,dt):
        self.wheelDia = wheelDia
        self.centerDist = centerDist
        self.dt = dt

    def position(self,leftVel,rightVel,prevX,prevY,prevTheta):

        centerVel = (leftVel + rightVel)/2
        dTheta = (rightVel - leftVel)/self.centerDist
        
        X = prevX + centerVel*np.cos( prevTheta + dTheta/2 )*self.dt
        Y = prevY + centerVel*np.sin( prevTheta + dTheta/2 )*self.dt
        Theta = prevTheta + dTheta*self.dt
        
        return(X,Y,Theta)

class DistanceController(object):
    def __init__(self,gainP,gainA,satUpper,satLower):
        self.gainP = gainP
        self.gainA = gainA
        self.satUpper = satUpper
        self.satLower = satLower


    def regulatorAruco(self,setP,xPos,yPos,zPos,idCamera,idControl,t,tTurn):
        
        #The robot is inline with the x vector of the arucomarker and 
        # only needs to move towards it in a straight line.
        if (idCamera == idControl):
            #The camera has detected the right ArUcomarker
            print('Detected ArUcomarker nr.',idCamera)
            absVector = np.sqrt(np.power(xPos,2) + np.power(yPos,2) + np.power(zPos,2))
            theta = np.arctan(xPos/(zPos))
            outputPos = (setP - absVector)*self.gainP
            outputTheta = (0 - theta)*self.gainA 
            

            if outputPos > self.satUpper:
                outputPos = self.satUpper

            elif outputPos < self.satLower:
                outputPos = self.satLower

            else:
                outputPos = outputPos

        elif (idCamera != idControl):
            outputTheta = 0
            outputPos = 0
        


        leftMotor = outputPos + outputTheta
        rightMotor = outputPos - outputTheta
        
        return(leftMotor,rightMotor)

    def regulatorBerry(self,setP,xPos,yPos,zPos):

        absVector = np.sqrt(np.power(xPos,2) + np.power(yPos,2) + np.power(zPos,2))
        theta = np.arctan(xPos/(zPos))
        outputPos = (setP - absVector)*self.gainP
        outputTheta = (0 - theta)*self.gainA 
        

        if outputPos > self.satUpper:
            outputPos = self.satUpper

        elif outputPos < self.satLower:
            outputPos = self.satLower

        else:
            outputPos = outputPos

        leftMotor = outputPos + outputTheta
        rightMotor = outputPos - outputTheta

        return(leftMotor,rightMotor)     

    def manual(self,tMove,tTurn,t,tInstance):

        outputTheta = 0
        outputPos = 0

        #Function for making the robot turn on command, 
        # the desired turn angle is set by allowing the robot to turn in a finith amount of time.
        turn = np.sign(tTurn)
        forward = np.sign(tMove)
        tEnd = tInstance + np.abs(tTurn) + np.abs(tMove)

        if (turn == 1):
            print('Turning clockwise')
            if (t <= tEnd):
                outputTheta = -15
                outputPos = 0

        
        elif (turn == -1):
            print('Turning counterclockwise')
            if (t <= tEnd):
                outputTheta = 15
                outputPos = 0

        elif (forward == 1):
            print('Moving forwards')
            if (t <= tEnd):
                outputTheta = 0
                outputPos = -15
        
        elif (forward == -1):
            print('Moving backwards')
            if (t <= tEnd):
                outputTheta = 0
                outputPos = 15

        leftMotor = outputPos + outputTheta
        rightMotor = outputPos - outputTheta
        
        return(leftMotor,rightMotor,tEnd)
         
def AbsVector(x,y,z):

    absoluteValue= np.sqrt(np.power(x,2) + np.power(y,2) + np.power(z,2))
    
    return(absoluteValue)




if __name__ == '__main__':
    try:
        # Init ROS node
        rospy.init_node('mainController', anonymous=True)

        # Web Joysticks
        #leftJoystick = Joystick()
        #rightJoystick = Joystick()

        # Instance of ArucoMarker class
        arucoData = ArucoData()


        # Instance of DistanceController class
            # gainP = 500
            # gainA = 10
            # upperSat = 10
            # lowerSat = -10
        distanceController = DistanceController(500,10,15,-15)

        # Publishers
        pub_servoSetpoints = rospy.Publisher('servoSetpoints', ServoSetpoints, queue_size=1)
        pub_strawberry_detection = rospy.Publisher('strawberry_detection', Image, queue_size=1)
    
        # Strawberry detector
        intrinsicCalibration =  np.load('%s/catkin_ws/src/mas507/data/intrinsicCalibration.npz' % (os.path.expanduser("~")))
        strawberryDetector = StrawberryDetector(pub_strawberry_detection, intrinsicCalibration['mtx'], intrinsicCalibration['dist'])

        # Subscribers
        #sub_leftJoystick = rospy.Subscriber('webJoystickLeft', WebJoystick, leftJoystick.callback)
        #sub_rightJoystick = rospy.Subscriber('webJoystickRight', WebJoystick, rightJoystick.callback)
        sub_arucoData = rospy.Subscriber('arucodata', ArucoMarker, arucoData.callback)
        sub_calibrated = rospy.Subscriber('image_calibrated', Image, strawberryDetector.callback)


        # Start Synchronous ROS node execution
        arucoNr = 1
        armSet = 200
        nCase = 1
        t = 0
        t_1 = 0
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():

            #print('Main Controller loop running') # Used for debugging

            xBerry,yBerry,zBerry = strawberryDetector.x,strawberryDetector.y,strawberryDetector.z

            if (arucoNr == 1):
                absID = AbsVector(arucoData.x1,arucoData.y1,arucoData.z1)
                arucoX, arucoY, arucoZ, arucoID = arucoData.x1,arucoData.y1,arucoData.z1,arucoData.id1

            elif (arucoNr == 2):
                absID = AbsVector(arucoData.x2,arucoData.y2,arucoData.z2)
                arucoX, arucoY, arucoZ, arucoID = arucoData.x2,arucoData.y2,arucoData.z2,arucoData.id2

            elif (arucoNr == 3):
                absID = AbsVector(arucoData.x3,arucoData.y3,arucoData.z3)
                arucoX, arucoY, arucoZ, arucoID = arucoData.x3,arucoData.y3,arucoData.z3,arucoData.id3

            ## First plant
            # Move to first plant
            if (nCase == 1): 
                velLeft,velRight = distanceController.regulatorAruco(0.99, arucoX, arucoY, arucoZ, arucoID, arucoNr, t, t_1)
                print(velLeft,velRight,nCase,absID)
                armSet = 200

                if (0.97 < absID < 1.01):
                    t_2 = t
                    nCase = 2
                
            # Turn 90 degress CW
            if (nCase == 2): 
                velLeft,velRight,tEnd = distanceController.manual(0,1.3,t,t_2)
                print(velLeft,velRight,nCase,absID)
                
                if (t>tEnd):
                    t_3 = t
                    nCase = 3

            # Wait for 2 seconds
            if (nCase == 3): 
                velLeft,velRight,tEnd = 0,0,(t_3 + 2)
                
                if (t>tEnd):
                    t_4 = t
                    nCase = 4

            # Look for red strawberry
            if (nCase == 4):
                absBerry = AbsVector(xBerry,yBerry,zBerry)
                print(absBerry)

                if (absBerry !=0) and (absBerry>0.08):
                    print("Red strawberry detected")
                    velLeft,velRight = distanceController.regulatorBerry(0.12,xBerry,yBerry,zBerry)

                    if (0.10 < absBerry < 0.14):
                        t_5 = t
                        nCase = 5
                        
                else:
                    print("Red strawberry not present")
                    t_7 = t
                    nCase = 7

            # Pick berry
            if (nCase == 5):
                
                armSet = 540
                absBerry = 0
                print("Picking strawberry")
                tEnd = t_5 + 1

                if (t>tEnd):
                    t_6 = t
                    nCase = 6
            
            # Move backwards
            if (nCase == 6): 
                velLeft,velRight,tEnd = distanceController.manual(-0.8,0,t,t_6)
                print(velLeft,velRight,nCase,absID)
            
                if (t>tEnd):
                    t_7 = t
                    nCase = 7

            # Turn 90 degrees CCW
            if (nCase == 7): 
                velLeft,velRight,tEnd = distanceController.manual(0,-1.175,t,t_7)
                print(velLeft,velRight,nCase,absID)
                armSet = 200
            
                if (t>tEnd):
                    t_8 = t
                    nCase = 8

            ## Second plant
            # Move to next plant 
            if (nCase == 8): 
                velLeft,velRight = distanceController.regulatorAruco(0.86,arucoX, arucoY, arucoZ, arucoID, arucoNr, t, t_8)
                print(velLeft,velRight,nCase,absID)

                if (0.84 < absID < 0.88):
                    t_9= t
                    nCase = 9

            # Turn 90 degress CW
            if (nCase == 9): 
                velLeft,velRight,tEnd = distanceController.manual(0,1.35,t,t_9)
                print(velLeft,velRight,nCase,absID)
                
                if (t>tEnd):
                    t_10 = t
                    nCase = 10

            # Wait for 2 seconds
            if (nCase == 10): 
                velLeft,velRight,tEnd = 0,0,(t_10 + 2)
                
                if (t>tEnd):
                    t_11 = t
                    nCase = 11

            # Look for red strawberry
            if (nCase == 11):
                absBerry = AbsVector(xBerry,yBerry,zBerry)
                print(absBerry)

                if (absBerry !=0) and (absBerry>0.08):
                    print("Red strawberry detected")
                    velLeft,velRight = distanceController.regulatorBerry(0.115,xBerry,yBerry,zBerry)

                    if (0.10 < absBerry < 0.14):
                        t_12 = t
                        nCase = 12
                        
                else:
                    print("Red strawberry not present")
                    t_14 = t
                    nCase = 14

            # Pick berry
            if (nCase == 12):
                
                armSet = 540
                absBerry = 0
                print("Picking strawberry")
                tEnd = t_12 + 1

                if (t>tEnd):
                    t_13 = t
                    nCase = 13
            
            # Move backwards
            if (nCase == 13): 
                velLeft,velRight,tEnd = distanceController.manual(-0.8,0,t,t_13)
                print(velLeft,velRight,nCase,absID)
            
                if (t>tEnd):
                    t_14 = t
                    nCase = 14

            # Turn 90 degrees CCW
            if (nCase == 14): 
                velLeft,velRight,tEnd = distanceController.manual(0,-1.175,t,t_14)
                print(velLeft,velRight,nCase,absID)
                armSet = 200
            
                if (t>tEnd):
                    t_15 = t
                    nCase = 15

            ## Third plant 
            # Move to next plant 
            if (nCase == 15): 
                velLeft,velRight = distanceController.regulatorAruco(0.72,arucoX, arucoY, arucoZ, arucoID,arucoNr,t,t_15)
                print(velLeft,velRight,nCase,absID)

                if (0.70 < absID < 0.74):
                    t_16= t
                    nCase = 16

            # Turn 90 degress CW
            if (nCase == 16): 
                velLeft,velRight,tEnd = distanceController.manual(0,1.3,t,t_16)
                print(velLeft,velRight,nCase,absID)
                
                if (t>tEnd):
                    t_17 = t
                    nCase = 17

            # Wait for 2 seconds
            if (nCase == 17): 
                velLeft,velRight,tEnd = 0,0,(t_17 + 2)
                
                if (t>tEnd):
                    t_18 = t
                    nCase = 18

            # Look for red strawberry
            if (nCase == 18):
                absBerry = AbsVector(xBerry,yBerry,zBerry)
                print(absBerry)

                if (absBerry !=0) and (absBerry>0.08):
                    print("Red strawberry detected")
                    velLeft,velRight = distanceController.regulatorBerry(0.12,xBerry,yBerry,zBerry)

                    if (0.10 < absBerry < 0.14):
                        t_19 = t
                        nCase = 19
                        
                else:
                    print("Red strawberry not present")
                    t_21 = t
                    nCase = 21

            # Pick berry
            if (nCase == 19):
                
                armSet = 540
                absBerry = 0
                print("Picking strawberry")
                tEnd = t_19 + 1

                if (t>tEnd):
                    t_20 = t
                    nCase = 20
            
            # Move backwards
            if (nCase == 20): 
                velLeft,velRight,tEnd = distanceController.manual(-0.8,0,t,t_20)
                print(velLeft,velRight,nCase,absID)
            
                if (t>tEnd):
                    t_21 = t
                    nCase = 21

            # Turn 90 degrees CCW
            if (nCase == 21): 
                velLeft,velRight,tEnd = distanceController.manual(0,-1.175,t,t_21)
                print(velLeft,velRight,nCase,absID)
                armSet = 200
            
                if (t>tEnd):
                    t_22 = t
                    nCase = 22

            ## Forth plant
            # Move to next plant 
            if (nCase == 22): 
                velLeft,velRight = distanceController.regulatorAruco(0.58,arucoX, arucoY, arucoZ, arucoID,arucoNr,t,t_22)
                print(velLeft,velRight,nCase,absID)

                if (0.57 < absID < 0.59):
                    t_23 = t
                    nCase = 23

            # Turn 90 degress CW
            if (nCase == 23): 
                velLeft,velRight,tEnd = distanceController.manual(0,1.45,t,t_23)
                print(velLeft,velRight,nCase,absID)
                
                if (t>tEnd):
                    t_24 = t
                    nCase = 24

            # Wait for 2 seconds
            if (nCase == 24): 
                velLeft,velRight,tEnd = 0,0,(t_24 + 2)
                
                if (t>tEnd):
                    t_25 = t
                    nCase = 25

            # Look for red strawberry
            if (nCase == 25):
                absBerry = AbsVector(xBerry,yBerry,zBerry)
                print(absBerry)

                if (absBerry !=0) and (absBerry>0.08):
                    print("Red strawberry detected")
                    velLeft,velRight = distanceController.regulatorBerry(0.115,xBerry,yBerry,zBerry)

                    if (0.10 < absBerry < 0.14):
                        t_26 = t
                        nCase = 26
                        
                else:
                    print("Red strawberry not present")
                    t_28 = t
                    nCase = 28

            # Pick berry
            if (nCase == 26):
                
                armSet = 540
                absBerry = 0
                print("Picking strawberry")
                tEnd = t_26 + 1

                if (t>tEnd):
                    t_27 = t
                    nCase = 27
            
            # Move backwards
            if (nCase == 27): 
                velLeft,velRight,tEnd = distanceController.manual(-0.8,0,t,t_27)
                print(velLeft,velRight,nCase,absID)
            
                if (t>tEnd):
                    t_28 = t
                    nCase = 28

            # Turn 90 degrees CCW
            if (nCase == 28): 
                velLeft,velRight,tEnd = distanceController.manual(0,-1.175,t,t_28)
                print(velLeft,velRight,nCase,absID)
                armSet = 200
            
                if (t>tEnd):
                    t_29 = t
                    nCase = 29

            ## Fifth plant
            # Move to next plant 
            if (nCase == 29): 
                velLeft,velRight = distanceController.regulatorAruco(0.42,arucoX, arucoY, arucoZ, arucoID,arucoNr, t, t_29)
                print(velLeft,velRight,nCase,absID)

                if (0.405 < absID < 0.425):
                    t_30 = t
                    nCase = 30

            # Turn 90 degress CW
            if (nCase == 30): 
                velLeft,velRight,tEnd = distanceController.manual(0,1.35,t,t_30)
                print(velLeft,velRight,nCase,absID)
                
                if (t>tEnd):
                    t_31 = t
                    nCase = 31

            # Wait for 2 seconds
            if (nCase == 31): 
                velLeft,velRight,tEnd = 0,0,(t_31 + 2)
                
                if (t>tEnd):
                    t_32 = t
                    nCase = 32

            # Look for red strawberry
            if (nCase == 32):
                absBerry = AbsVector(xBerry,yBerry,zBerry)
                print(absBerry)

                if (absBerry !=0) and (absBerry>0.08):
                    print("Red strawberry detected")
                    velLeft,velRight = distanceController.regulatorBerry(0.115,xBerry,yBerry,zBerry)

                    if (0.10 < absBerry < 0.14):
                        t_33 = t
                        nCase = 33
                        
                else:
                    print("Red strawberry not present")
                    t_35 = t
                    nCase = 35

            # Pick berry
            if (nCase == 33):
                
                armSet = 540
                absBerry = 0
                print("Picking strawberry")
                tEnd = t_33 + 1

                if (t>tEnd):
                    t_34 = t
                    nCase = 34
            
            # Move backwards
            if (nCase == 34): 
                velLeft,velRight,tEnd = distanceController.manual(-0.9,0,t,t_34)
                print(velLeft,velRight,nCase,absID)
            
                if (t>tEnd):
                    t_35 = t
                    nCase = 35

            # Turn 90 degrees CCW
            if (nCase == 35): 
                velLeft,velRight,tEnd = distanceController.manual(0,-1.175,t,t_35)
                print(velLeft,velRight,nCase,absID)
                
            
                if (t>tEnd):
                    t_36 = t
                    nCase = 36
                    

            # Drive up to ArUco marker
            if (nCase == 36):
                velLeft,velRight = distanceController.regulatorAruco(0.25, arucoX, arucoY, arucoZ, arucoID,arucoNr,t,t_36)
                print(absID)
                armSet = 540
                if (0.24 < absID < 0.255) and (arucoNr == 1):
                    t_37 = t
                    nCase = 37
                    arucoNr = 2

                elif (0.245 < absID < 0.255) and (arucoNr == 3):
                    velLeft,velRight = 0,0
                    print("Strawberry picking finished")



            # Turn 90 degrees CW
            if (nCase == 37): 
                velLeft,velRight,tEnd = distanceController.manual(0,1.175,t,t_37)
                print(velLeft,velRight,nCase,absID)
            
                if (t>tEnd):
                    t_38 = t
                    nCase = 38


            ## Drive to ArUco marker number two
            if (nCase == 38) and (arucoNr == 2):
                velLeft,velRight = distanceController.regulatorAruco(0.24, arucoX, arucoY, arucoZ, arucoID,arucoNr,t,t_38)
                print("Drive to ArUco marker number two")
                if (0.23 < absID < 0.25) and (arucoID == 2):
                    t_39 = t
                    nCase = 39
                    armSet = 200
                


            # Turn 90 degrees CW
            if (nCase == 39): 
                velLeft,velRight,tEnd = distanceController.manual(0,1.175,t,t_39)
                print(velLeft,velRight,nCase,absID)
            
                if (t>tEnd):
                    t_1 = t
                    nCase = 1
                    arucoNr = 3


        

            #ServoSetpoints
            servoSetpoints = ServoSetpoints()
            servoSetpoints.leftWheel  = 338 - 3 - velLeft #- rightJoystick.y/2 + leftJoystick.x/7
            servoSetpoints.rightWheel = 329 + 3 + velRight #+ rightJoystick.y/2 + leftJoystick.x/7
            servoSetpoints.servo1 =  armSet

            #Publish servoSetpoints
            pub_servoSetpoints.publish(servoSetpoints)
            writer.writerow([t,arucoData.z1])
            
            t = t + 0.1

            # Sleep remaining time
            rate.sleep()


    except rospy.ROSInterruptException:
        passgit 